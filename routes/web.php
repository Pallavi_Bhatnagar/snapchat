<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

//use Illuminate\Http\Request;
//
//Route::get('/search', function (Request $request) {
//    return App\User::search($request->search)->get();
//});
//Route::get('searchTextName/{name}', 'profileController@home');

//Route::get('forgotPassword{token?}', 'Auth\ForgotPasswordController@passwordForgot');
//Route::post('password/email', 'Auth\ResetPasswordController@sentResetPasswordMail');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::get('register/verify/{confirmationCode}','registrationController@confirm');

Route::get('searchtextname', 'searchingController@search');

Route::any('fetch/friends/{query}','searchingController@fetchUsersByTypeahead' );

Route::post('friendsProfile', 'profileController@showFriendsProfile');

Route::get('auth/register', 'registrationController@getRegister');

Route::post('auth', 'registrationController@postRegister');

Route::get('showusers', 'friendController@showAllUser');

Route::get('editProfile', 'profileController@editYourProfile');

Route::post('saveChanges/{user}', 'profileController@saveEditedUserProfile');

Route::get('login', 'registrationController@getLogin');

Route::any('profilepage','profileController@show' );

Route::post('postlogin', 'registrationController@postLogin');

Route::post('profilePage/update', 'profileController@store' );

Route::get('messagebox/sendMessages', 'messageController@showFriendList');

Route::get('friendrequest/{id}/{name}', 'friendController@makeFriends');

Route::get('acceptfriend/{id}/{name}', 'friendController@index');

Route::post('send/messages', 'messageController@sendMessage');

Route::get('send/messages/to/many', 'messageController@sendMessageToMany');

Route::post('send/messages/many', 'messageController@postsendMessageToMany');

Route::get('chat/{id}/{friendName}', 'profileController@chatbox');

Route::get('messagebox/yourmessage', 'messageController@getSentMessage');

Route::get('logout', 'registrationController@getLogout');

//Route::get('read/message/{id}', 'messageController@getReadMessage');

Route::get('delete/read/{id}', 'messageController@autoDeleteMessage');




