<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;

class imageStore extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'imagePath',
        'user_id'
    ];

    public static function getValueFromUser($imagePath)
    {
        $imageExistOrNot = imageStore::where('user_id', Auth::user()->id)->value('imagePath');

        if($imageExistOrNot == null)
        {
            return static::create([
                'imagePath' => $imagePath,
                'user_id' => Auth::user()->id
            ]);
        }
        else
        {
            return static::where('user_id', Auth::user()->id)->update([
                'imagePath' => $imagePath,
                'user_id' => Auth::user()->id
            ]);
        }

    }

    public function fetchImageViaUserId($userid)
    {
       return imageStore::where('user_id', $userid)->value('imagePath');
    }
}
