<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class friend_notification extends Model
{
    protected $fillable = [
        'user_id',
        'user_name',
        'friend_id',
        'friend_name'
    ];
}
