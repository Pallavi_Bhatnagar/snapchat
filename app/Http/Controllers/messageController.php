<?php

namespace App\Http\Controllers;

use App\imageStore;
use App\Sendpics;
use App\User;
use App\FriendList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Messages;
use App\Http\Requests\PicRequest;
use Carbon\Carbon;
use App\Http\Requests;

class messageController extends Controller
{

    public function sendMessage(PicRequest $request)
    {
//        dd($request->all());
        $file = $request->file('pics_path');
        $picspath = 'message';
        $too = $file->move($picspath, $file->getClientOriginalName());

        Messages::create([
            'pics_path' => $picspath,
            'user_id' => Auth::user()->id,
            'sender_name' => Auth::user()->name,
            'messages' => $request->messages,
            'pics_path' => $too,
            'set_duration' => $request->set_duration,
            'To_user_id' => $request->To_user_id,
            'FriendName' => $request->FriendName
        ]);
        return redirect()->back();
    }

    public function sendMessageToMany()
    {
        $friendList = FriendList::where('user_id', Auth::user()->id)->get();
        $friendid = FriendList::get(['friend_id']);
        $friendname = FriendList::get(['friendName']);


        return view('messagebox/messagesSendToMany', compact('friendList', 'friendid', 'friendname'));
    }

    public function postsendMessageToMany(Request $request)
    {
        $checkFriends = FriendList::where('user_id', Auth::user()->id)->get(['friend_id']);

        //getting selected checkbox values
        $input = $request->input('checkbox');

        $file = $request->file('pics_path');
        $picspath = 'message';
        $too = $file->move($picspath, $file->getClientOriginalName());

        foreach ($input as $toUserId) {
            $friendName = User::where('id', $toUserId)->value('name');

            Messages::create(['user_id' => Auth::user()->id,
                'sender_name' => Auth::user()->name,
                'messages' => $request->messages,
                'pics_path' => $too,
                'set_duration' => $request->set_duration,
                'To_user_id' => $toUserId,
                'FriendName' => $friendName
            ]);
        }

        return redirect()->back();
    }

    public function showFriendList()
    {
        $friendRequest = FriendList::where('user_id', Auth::user()->id)->get();
        $friendRequestId = FriendList::get(['friend_id']);
        $friendRequests = User::all();
        return view('messagebox/sendMessages', compact('friendRequests', 'friendRequest', 'friendRequestId'));
    }

    public function getSentMessage()
    {
        $messageSenders = Messages::where('user_id', Auth::user()->id)->get();

        return view('messagebox/yourMessage', compact('messageSenders'));
    }

//    public function getReadMessage($id)
//    {
////        dd('hello');
//        $fullMessage = Messages::where('id',$id)
//            ->where('To_user_id', Auth::user()->id)
//            ->value('messages');
//
//        Messages::where('id', $id)
//            ->update(['read_or_not' => '1']);
//
////        dd($id);
//        Messages::where('id',$id)
//            ->where('To_user_id', Auth::user()->id)
//            ->delete();
//
//       $messageSender = Messages::where('user_id', Auth::user()->id)->value('messages');
//
//        return view('readMessage', compact('fullMessage'));
//    }
//dd(Messages::whereRaw('date >= NOW()'));
    public function autoDeleteMessage($id)
    {
//        dd($id);
        $var = Messages::where('id', $id)
//            ->where('To_user_id', Auth::user()->id)
            ->delete();
        return back();
    }
}



