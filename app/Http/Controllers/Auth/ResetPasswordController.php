<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

//    public function sentResetPasswordMail(Request $request)
//    {
//        $sendemail = array(
//            'email'=>$request['email']
//        );
//
//        Mail::send('email.verifyUsers', $sendemail, function ($message) use($sendemail) {
////            $message->to(Input::get('email'), subject('verify your email address');
//            $message->to(Input::get('email'), Input::get('name'))->subject('verify your email address');
//        });
//
//        Session::flash('flash_success', 'Please check your email.');
//
//        return redirect()->back();
//    }
//
//    public function resetPassword()
//    {
//
//    }

}
