<?php

namespace App\Http\Controllers;

use App\FriendList;
use App\User;
use App\friend_notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class friendController extends Controller
{
    public function makeFriends(Request $request)
    {
//        $friendIds = $id;
        FriendList::create([
            'user_id' => Auth::user()->id,
            'user_name' => Auth::user()->name,
            'friend_id' => $request->id,
            'friendName' => $request->name
        ]);

        return redirect()->back();
    }

    public function showRequest()
    {
        return view('friends/friendRequest');
    }

    public function showAllUser()
    {
        $showUsers = User::all();
        return view('friends/showAllUsers', compact('showUsers'));
    }

    public function index(Request $request)
    {
        FriendList::acceptFriend();

        friend_notification::create([
        'user_id' => Auth::user()->id,
        'user_name' => Auth::user()->name,
        'friend_id' => $request->id,
        'friend_name' => $request->name
    ]);
        return back();
    }
}