<?php

namespace App\Http\Controllers;

use App\FriendList;
use App\Messages;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ImageStore;
use App\Http\Requests;

class profileController extends Controller
{
    public function showFriendsProfile(Request $request)
    {
        $images = imageStore::where('user_id', Auth::user()->id)->get();
        $friendRequest = FriendList::where('friend_id', Auth::user()->id)->get();
        return view('profiles.friendsProfile', compact('images', 'friendRequest'));
    }

    public function editYourProfile()
    {
        $fillUsers = User::where('id', Auth::user()->id)->get();

        return view('profiles/editProfile', compact('fillUsers'));
    }

    public function saveEditedUserProfile(Request $request, User $user)
    {
        $user->update($request->all());
        return back();
    }

    public function show()
    {
        if (Auth::check()) {
            $images = ImageStore::where('user_id', Auth::user()->id)
                ->get();
            $friends = User::all()->take(2);

            $userImages = ImageStore::all();

            $friendRequestId = FriendList::where('are_friends',1)
                ->pluck('friend_id');
//            $friendRequest = FriendList::whereIn('friend_id',$friendRequestId)->get();
            $friendRequest = FriendList::all();

            $message = Messages::where('To_user_id',Auth::user()->id)->get();

            $getId= Messages::where('user_id',Auth::user()->id)->get();

            return view('profilePage', compact('images', 'friends', 'friendRequest', 'message', 'friendRequestId', 'friendRequestShow', 'fullMessage'));
        }
    }

    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            dd($request->hasFile('image'));
            $file = $request['image'];
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

            $imagePath = 'images/' . $timestamp . '-' . $file->getClientOriginalName();
            move_uploaded_file($file, $imagePath);
            ImageStore::getValueFromUser($imagePath);
        }
        return redirect()->back();
    }

    public function messagebox()
    {
        return view('messagebox/sendMessages');
    }

    public function chatbox($id,$friendName)
    {
        $toUserId = $id;
        $sendMessageToFriend = $friendName;

        return view('messagebox/chatbox', compact('toUserId', 'friendName'));
    }

}