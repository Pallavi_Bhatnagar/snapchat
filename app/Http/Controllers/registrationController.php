<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
//use App\Http\Controllers\LoginController;
use App\Http\Requests\registrationRequest;
use App\Http\Requests\loginRequest;
use App\Http\Controllers\Redirect;
use App\User;
use App\imageStore;
use App\Messages;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class registrationController extends Controller
{
    public function getRegister()
    {
        return view('auth.register');
    }

    public function postRegister(registrationRequest $request)
    {
        $confirmation_code = str_random(10);
        $emailContent = array(
            'confirmation_code'=>$confirmation_code,
            'email'=>$request['email'],
        );
//        dd($request);
        User::create([
            'name' => $request['name'],
            'gender' => $request['gender'],
            'address' => $request['address'],
            'phone_no' => $request['phone_no'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'confirmation_code' => $confirmation_code,
        ]);

//       $getToken =  User::where('confirmation_code',$confirmation_code)->
//            where('email',$request['email'])->value('confirmation_code');
//        dd($getToken);
//        dd($confirmation_code);
        Mail::send('email.verifyUsers', $emailContent, function ($message) use($emailContent) {
//            $message->to(Input::get('email'), subject('verify your email address');
            $message->to(Input::get('email'), Input::get('name'))->subject('verify your email address');
        });

        Session::flash('flash_success','Thanks for signing up! Please check your email.');

        return redirect('/');
    }

    public function confirm($confirmation_code)
    {
        if (!$confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

//        $user = User::whereConfirmationCode('confirmation_code',$confirmation_code)->first();
//        $user = User::where('confirmation_code',$confirmation_code)->first();
//        dd($user);

//        if (!$user) {
//            throw new InvalidConfirmationCodeException;
//        }

        User::where('confirmation_code', $confirmation_code)
            ->update(['confirmed' => 1, 'confirmation_code' => '']);


        Session::flash('message', 'You have successfully verified your email.');

        return view('');
    }

    public function getLogin()
    {
        return view('home');
    }

    public function postLogin(Request $request)
    {
//        dd($request->all());

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'confirmed' => 1
        ];

        if (!Auth::attempt($credentials)) {
            return Redirect()->back()
                ->withInput()
                ->withErrors([
                    'credentials' => 'We were unable to sign you in.'
                ]);
        }
        elseif (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            {
                $images = ImageStore::where('user_id', Auth::user()->id)->get();
//             Messages::where('user_id', Auth::user()->id)
//                        ->update(['read_or_not' => '1']);
                return redirect('profilepage');
            }

        }
//        return redirect('/')
//            ->withInput($request->only('email'))
//            ->withErrors([
//                'email' => 'These credentials do not match our records.',
//            ]);

//        return redirect('upload/profile/pic');
    }


    public function getLogout()
    {
        auth()->logout();

//        Messages::where('user_id', Auth::user()->id)
//            ->update(['read_or_not' => '0']);

        return redirect('/');
    }

}
