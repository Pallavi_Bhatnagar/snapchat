<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ImageStore;
use App\FriendList;
use App\Http\Requests;
use App\User;

class searchingController extends Controller
{
    public function search(Request $request)
    {
//        $searchableText = $request['search'];
//        $search = User::where('name', $searchableText)->get();
//        dd('hello');
//        $orders = User::search('Tweety')->where('user_id', 5)->get();
//        if(($user_id = $request->get('user_id')))
//        {
//            $searchNames = User::where('user_id', $user_id)->orderBy('id', 'desc')->paginate(5);
//        }
//        else{
//            $searchNames = User::orderBy('id', 'desc')->paginate(5);
//        }
                $images = ImageStore::where('user_id', Auth::user()->id)
            ->get();
        $friendRequest = FriendList::where('user_id',Auth::user()->id)->get();

//        $searchNames = User::where(function($query) use ($request) {
//            filter by selected group
//            if (($user_id = $request->get('user_id'))) {
//                $query->where('user_id', $user_id);
//            }

            //filter by keyword entered
//            if(($term = $request->get('search'))){
//                $query=User::where('name', 'like', '%'. $term. '%')->get()->pluck('name')->all();
//
////                $query->orwhere('email', 'like', '%'. $term. '%')->pluck('name');
//
//            }
//        });
            $term = $request->get('search');
            $query = User::where('name', 'like', '%' . $term . '%')->get(['name']);
//                ->orderBy('id', 'desc')
//                ->paginate(5);
//        dd(($query));

            return view('profiles/friendsProfile', compact('images', 'friendRequest', 'query'));

    }

    public function fetchUsersByTypeahead($query)
    {
        $search = User::where('name', 'like', '%'.$query. '%')->pluck('name');
        echo json_encode($search);
    }
}