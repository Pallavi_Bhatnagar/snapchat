<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FriendList extends Model
{
    protected $table = 'friendlist';
    protected $fillable = [
        'user_id',
        'user_name',
        'friend_id',
        'friendName',
        'are_friends'
    ];

    public static function acceptFriend()
    {
        FriendList::where('friend_id', Auth::user()->id)
            ->update(['are_friends' => 1]);
    }

}
