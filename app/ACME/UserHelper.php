<?php
namespace App\ACME;

use App\imageStore;
use App\Messages;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\FriendList;
use App\friend_notification;

class UserHelper
{
//    public function showFriendRequest()
//    {
//       return $friendRequest = FriendList::where('user_id',Auth::user()->id)->get();
//    }

    public static function receiveFriendRequest()
    {
//        dd(Auth::user()->id);
       return FriendList::where('friend_id', Auth::user()->id)
                        ->where('are_friends',0)
                        ->get();
//        return FriendList::where('user_id', $xyz)->get();
    }

    public function countList()
    {
       return $count = FriendList::where('friend_id', Auth::user()->id)
                        ->where('are_friends',0)
                        ->count();
    }

    public static function showNotification()
    {
       return friend_notification::where('friend_id', Auth::user()->id)->get();
    }

    public function countNotifications()
    {
        return $countNotification = friend_notification::where('friend_id', Auth::user()->id)
            ->count();
    }

    public function addToFriendList()
    {
//       return $friendRequest = FriendList::where('friend_id',Auth::user()->id)->get();
//       return FriendList::where('user_id',Auth::user()->id)->where('are_friends' , 1)->get();
       return  FriendList::all();
    }

    public function getUserImage()
    {
        return imageStore::where('user_id', Auth::user()->id)->value('imagePath');
    }

    public function getFriendPicAsMessage($id,$touserId)
    {
//        dd($iii);
        return Messages::where('id', $id)->where('To_user_id', $touserId)->value('pics_path');
//        return Messages::all();
    }
}

