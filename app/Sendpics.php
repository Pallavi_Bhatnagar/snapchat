<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sendpics extends Model
{
    protected $fillable = [
      'user_id', 'sender_name', 'pics_path', 'friend_id', 'friend_name'
    ];

}
