<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class deleteMessageOnClick extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete messages after 2 mins on click';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($createdAt)
    {
        Messages::where('read_or_not',1)
            ->where('created_at', $createdAt)
            ->get();
    }

}
