<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $table = 'messages';
    protected $fillable = [
        'user_id',
        'sender_name',
        'messages',
        'pics_path',
        'set_duration',
        'To_user_id',
        'FriendName',
        'read_or_not'
    ];
}
