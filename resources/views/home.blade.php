@if(Session::has('flash_success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
        {{ Session::get('flash_success') }}
    </div>
@endif

@extends('layout')
@section('title')
@stop
@section('css')
    <style>
        body{
            background-color: white;
        }

        .jumbotron{
            background-color: lightblue;
        }

        .centered-form{
            margin-top: 60px;
        }

        .centered-form .panel{
            background: rgba(255, 255, 255, 0.8);
            box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
        }

        #login-dp{
            min-width: 550px;
            padding: 14px 14px 0;
            overflow:hidden;
            background-color:rgba(255,255,255,.8);
        }
        #login-dp .help-block{
            font-size:12px
        }
        #login-dp .bottom{
            background-color:rgba(255,255,255,.8);
            border-top:1px solid #ddd;
            clear:both;
            padding:14px;
        }
        #login-dp .social-buttons{
            margin:12px 0
        }
        #login-dp .social-buttons a{
            width: 49%;
        }
        #login-dp .form-group {
            margin-bottom: 10px;
        }

    </style>
@stop
@section('content')

    <div class="jumbotron">
        <div>
            <img src="/snapchat-logo.png" class="img-responsive" alt="Responsive image" align="right">
        </div>

        <h1 class="projectHeading">SnapChat</h1>
        <p>
            This is a template showcasing the optional theme stylesheet included in Bootstrap.
            Use it as a starting point to create something more unique by building on or modifying it.
            Have Fun with <SnapChat class="."></SnapChat>
        </p>
        <ul class="nav navbar-nav navbar-left">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                <ul id="login-dp" class="dropdown-menu">
                    <li>
                        <div class="row">
                            <div class="col-md-12">
{{--                                @include('errors.list')--}}
                                Login
                                <form class="form" role="form" method="post" action="{{url('postlogin')}}" accept-charset="UTF-8" id="login-nav">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control" name="email" id="exampleInputEmail2" placeholder="Email address" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputPassword2">Password</label>
                                        <input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                        <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit"  class="btn btn-primary btn-block">Login</button>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> keep me logged-in
                                        </label><br>
                                        <a href="{{url('password/reset')}}">Forgot your password?</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                </ul>
        </ul>

            <a href="auth/register" class="btn btn-primary">sign up</a>

    </div>

@stop
@section('js')
    @stop
