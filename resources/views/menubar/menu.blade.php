@inject('imageObj', 'App\imageStore')
@inject('objRequest', App\ACME\UserHelper)
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SnapChat</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <sub><img src="{{url('../resources/views/images/icon.png')}}"
                                  alt="Responsive image" width="20" height="20" style="margin-left: 2px;"></sub>
                        <sup>
                            @if($objRequest->countList() != 0)

                            <span class="badge"
                                      style="background-color: red; margin-bottom: 6px;position: relative;right: 8px;">
                                    {{$objRequest->countList()}}
                            </span>
                            @endif

                        </sup>
                    </a>

                    @foreach($objRequest->receiveFriendRequest() as $listOfFriendRequest)
                        @if($listOfFriendRequest->friend_id == \Illuminate\Support\Facades\Auth::user()->id)
                            <ul class="dropdown-menu" style="width:260px;">
                                <li>
                                    {{--{{dd($objRequest->receiveFriendRequest())}}--}}

                                    {{--@if($listOfFriendRequest->friend_id)--}}
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img src="{{$imageObj->fetchImageViaUserId($listOfFriendRequest->user_id)}}"
                                                 width="30" height="30">
                                            {{ $listOfFriendRequest->user_name }}
                                        </div>
                                        <div class="col-md-3">
                                            <a href="{{url('acceptfriend')}}/{{ $listOfFriendRequest->user_id }}/{{ $listOfFriendRequest->user_name }}"
                                               class="btn btn-sm btn-info col-sm-offset-1">Accept</a>
                                        </div>
                                        <div>
                                            <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                                            <input type="hidden" name="user_name" value="{{\Illuminate\Support\Facades\Auth::user()->name}}">
                                            <input type="hidden" name="friend_id" value="{{$listOfFriendRequest->user_id}}">
                                            <input type="hidden" name="friend_name" value="{{$listOfFriendRequest->user_name}}">
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#" class="btn btn-sm btn-info">Deny</a>
                                        </div>

                                    </div>
                                    <br>
                                </li>
                            </ul>
                        @endif
                    @endforeach
                </li>
                {{--</form>--}}
                <li><a href="{{ url('profilepage') }}">Profile</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="glyphicon glyphicon-envelope" style="color: rgb(155,172,216)"></span><span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('messagebox/sendMessages') }}">Personal message</a></li>
                        <li><a href="{{ url('messagebox/yourmessage') }}">Sent messages</a></li>
                        <li><a href="{{ url('send/messages/to/many') }}">Send Message To Many</a></li>
                    </ul>
                </li>
            </ul>
            <div>
{{--                {{dd(\Illuminate\Support\Facades\Auth::user()->id)}}--}}
                {!! Form::Open(['url' => 'friendsProfile','class' => 'navbar-form navbar-left', 'enctype' => 'multipart/form-data', 'accept-charset'=>'utf-8']) !!}
                <div class="input-group">
                    <input type="text" id="typeahead" class="form-control typeahead" placeholder="Search friends"
                           autocomplete="off" spellcheck="false" name="typeahead">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>

            <script>
                $('input.typeahead').typeahead({
                    name: 'typeahead',
                    remote: document.location.origin + '/snapchat/public/fetch/friends/%QUERY',
                    limit: 15
                });
            </script>

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <sup>
                            <span class="badge"
                                  style="background-color: red; margin-bottom: 18px;position: relative;left: 8px;">
                                 {{$objRequest->countNotifications()}}
                            </span>
                        </sup>
                        <span class="glyphicon glyphicon-bell" style="color: rgb(155,172,216)"></span><span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                    {{--{{dd($objRequest->showNotification())}}--}}
                            @foreach($objRequest->showNotification() as $notify)
                                @if($notify->friend_id == \Illuminate\Support\Facades\Auth::user()->id)
                                <b>{{ $notify->user_name }}
                                    {{$notify->friend_name}}</b>
                                    {{$notify->notification_status}}<br>
                                @endif
                                @endforeach
                        </li>
                    </ul>

                        </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">{{\Illuminate\Support\Facades\Auth::user()->name}}
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('editProfile') }}">Edit Profile</a></li>
                        <li><a href="{{ url('logout') }}" class="glyphicon glyphicon-log-out"> Bye</a></li>
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li><a href="#">Separated link</a></li>--}}
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
