<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    {{--<link rel="stylesheet" href="{{url('myCss/style.css')}}">--}}
    <link href="{{url('css/allCode.css')}}" rel="stylesheet">

    @yield('css')
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{url('js/typeahead.min.js')}}" ></script>
<!-- Styles -->
@if(\Illuminate\Support\Facades\Auth::check())
@include('menubar.menu')

{{--@if(Session::has('flash_danger'))--}}
    {{--<div class="alert alert-danger">--}}
        {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>--}}
        {{--{{ Session::get('flash_danger') }}--}}
    {{--</div>--}}
{{--@endif--}}
@endif
<div class="container-fluid"  style="background-color: lightblue">
    @yield('content')
</div>


{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.1/vue-resource.min.js"></script>--}}
{{--<script src="{{url('js/snap.js')}}"></script>--}}
@yield('js')

</body>
</html>