@inject('Obj', 'App\imageStore')
@extends('layout')

@section('title')
    sent Messages
@stop
@section('content')
    <h2>{{ 'Your sent Messages' }}</h2><br>
        @foreach($messageSenders as $messageSendersLists)
            <div class="alert alert-info fade in">
            <b><img src="/snapchat/public/{{$Obj->fetchImageViaUserId($messageSendersLists->To_user_id)}}
                        "width="40" height="40">
                {{$messageSendersLists->FriendName}}:</b>
            {{$messageSendersLists->messages}} <br>
            </div>
        @endforeach
@stop





