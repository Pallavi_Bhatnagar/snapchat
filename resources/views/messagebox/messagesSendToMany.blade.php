@inject('Obj', 'App\ACME\UserHelper')

@extends('layout')
@section('title')
@stop
@section('css')
@stop

@section('content')
    <div class="container">
{{--    <form method="post" action="{{ url('send/messages/many') }}" class="form form-horizontal"--}}
          {{--xmlns="http://www.w3.org/1999/html">--}}
        {!! Form::open(['url'=>'send/messages/many', 'files'=>true, 'class'=>'form form-horizontal', 'method' =>'post']) !!}

    <div class="row col-md-12">
        <div class="col-md-6 col-md-offset-2">
{{--                {{csrf_field()}}--}}
                <div class="panel panel-default">
                    <div class="panel-body">
                                        <textarea class="form-control counted" name="messages"
                                                  placeholder="Type in your message" rows="5"
                                                  style="margin-bottom:10px;"></textarea>
                        <h6 class="pull-right" id="counter">320 characters remaining</h6>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::label('image', 'send an image') !!}
                            {!! Form::file('pics_path') !!}
                            {{--<input class="form-control glyphicon-open-file" type='file' name='filename'>--}}
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" name="user_id"
                                   value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                            <input type="hidden" name="sender_name"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->name }}">
{{--                            <input type="hidden" name="To_user_id" value="{{$friendid}}">--}}
                            {{--<input type="hidden" name="FriendName" value="{{$friendname}}">--}}
                            {!! Form::select('set_duration', ['15000' => '15 seconds', '30000' => '30 seconds', '60000' => '1 mins', '120000' => '2 mins', '300000' => '5 mins'])!!}

                            {!! Form::submit('Send Message', array( 'class'=>'btn btn-sm btn-primary')) !!}

                            {{--<input type="submit" class="btn btn-sm btn-primary" value='send message'>--}}
                        </div>
                    </div>
                </div>

        </div>

        <div class="col-md-4">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Friend List</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        @foreach($Obj->addToFriendList() as $friends)
                            @if($friends->are_friends == 1 && $friends->friend_id == \Illuminate\Support\Facades\Auth::user()->id)
                            {{ Form::checkbox('checkbox[]',$friends->user_id)}}  {{$friends->user_name}} <br>

                            @elseif($friends->user_id == \Illuminate\Support\Facades\Auth::user()->id && $friends->are_friends == 1)

                            {{ Form::checkbox('checkbox[]',$friends->friend_id)}} {{ $friends->friendName}}<br>
                        {{--{{Form::checkbox('checkbox[]', null, false, array('id'=>$friends->id))}} {{$friends->user_name}}--}}
                            @endif
                        @endforeach
                        <div class="form-group">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

        {!! Form::close() !!}
    {{--</form>--}}
    </div>
@stop
@section('js')
@stop
