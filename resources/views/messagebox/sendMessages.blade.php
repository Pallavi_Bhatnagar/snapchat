@inject('Obj', 'App\ACME\UserHelper')
@extends('layout')
@section('title')
@stop
@section('css')
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Friend List</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
{{--                                     {{dd($Obj->addToFriendList())}}--}}
                            @foreach($Obj->addToFriendList() as $friends)
{{--                                {{dd($friends)}}--}}
{{--                            @if($friends->are_friends == 1 && $friends->friend_id == \Illuminate\Support\Facades\Auth::user()->id)--}}
                            @if($friends->friend_id == \Illuminate\Support\Facades\Auth::user()->id && $friends->are_friends == 1)


                                <a class="bold"  href="{{ url('chat') }}/{{$friends->user_id}}/{{$friends->user_name}}" >{{ $friends->user_name }}</a><br>

                            @elseif($friends->user_id == \Illuminate\Support\Facades\Auth::user()->id && $friends->are_friends == 1)


                            {{----}}
                                <a class="bold"  href="{{ url('chat') }}/{{$friends->friend_id}}/{{$friends->friendName}}" >{{ $friends->friendName}}</a><br>
                            @endif
                            @endforeach

                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@stop
@section('js')
@stop
