@extends('layout')
@section('title')
@stop
@section('css')
@stop

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            {{--<form method="post" action="{{ url('send/messages') }}" class="form form-horizontal">--}}
                {!! Form::open(['url'=>'send/messages', 'files'=>true, 'class'=>'form form-horizontal', 'method' =>'post']) !!}
{{--                {{csrf_field()}}--}}
                <div class="panel panel-default">
                    <div class="panel-body">
                                        <textarea class="form-control counted" name="messages"
                                                  placeholder="Type in your message" rows="5"
                                                  style="margin-bottom:10px;"></textarea>
                        <h6 class="pull-right" id="counter">320 characters remaining</h6>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            {{--<div class="form-group">--}}
                                {!! Form::label('image', 'send an image') !!}
                                {!! Form::file('pics_path') !!}
                            @if($errors->has('pics_path'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('pics_path') }}</strong>
                                                </span>
                            @endif
                            {{--</div>--}}
                            {{--<input class="form-control glyphicon-open-file" value="" type='file' name='picture'>--}}
                        </div><br>
                        <div class="col-md-7">
                        {!! Form::select('set_duration', ['15000' => '15 seconds', '30000' => '30 seconds', '60000' => '1 mins', '120000' => '2 mins', '300000' => '5 mins'])!!}
                            </div> <br><br>
                        <div class="col-md-6">
                            <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                            <input type="hidden" name="sender_name" value="{{ \Illuminate\Support\Facades\Auth::user()->name}}">
                            <input type="hidden" name="To_user_id" value="{{$toUserId}}">
                            <input type="hidden" name="FriendName" value="{{$friendName}}">

                            {!! Form::submit('Send Message', array( 'class'=>'btn btn-sm btn-primary' )) !!}
                            {!! Form::close() !!}
                            {{--<input type="submit" class="btn btn-sm btn-primary" value='send message'>--}}
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
@stop
@section('js')
@stop
