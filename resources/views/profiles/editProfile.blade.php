@inject('objImage', App\ACME\UserHelper)
@extends('layout')
@section('title')
    Edit Profile
@stop
@section('css')

    @stop
@section('content')
<div class="container" style="color: black">
    <div class="panel" style="background-color: rgb(217,237,247);width: 100%;margin-top: 30px;">
    <h3 class="col-sm-offset-4">Edit Your Profile</h3>
    <hr>
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <div class="text-center">
                <img src="{{$objImage->getUserImage()}}" class="avatar img-circle" alt="avatar" width="120"; height="120">
                {{--<input class="form-control" type="file">--}}
                {{--<input class="btn btn-sm btn-primary" type="submit" value="upload">--}}
                {!! Form::open(['url'=>'profilePage/update', 'files'=>true]) !!}
                <div class="form-group">
                    {!! Form::label('image', 'Change an image') !!}
                    {!! Form::file('image') !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('upload', array( 'class'=>'btn btn-sm btn-primary' )) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <!-- edit form column -->
        <div class="col-md-5">

            @foreach($fillUsers as $fill)
            <form action="{{ url('saveChanges/'.$fill->id) }}" method="post" class="form-horizontal" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-lg-3 control-label">Your Name:</label>
                    <div class="col-lg-8">

                        <input class="form-control" name="name" value="{{$fill->name}}" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Gender:</label>
                    <div class="col-lg-8">
                        <input type="radio" name="gender" value="male"> Male
                        <input type="radio" name="gender" value="female"> Female
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Address:</label>
                    <div class="col-lg-8">
                        <input class="form-control" name="address" value="{{$fill->address}}" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Contact No.:</label>
                    <div class="col-lg-8">
                        <input class="form-control" name="phone_no" value="{{$fill->phone_no}}" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Email:</label>
                    <div class="col-lg-8">
                        <input class="form-control" name="email" value="{{$fill->email}}" type="text">
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">Password:</label>--}}
                    {{--<div class="col-md-8">--}}
                        {{--<input class="form-control" name="password" value="{{$fill->password}}" type="password">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">Confirm password:</label>--}}
                    {{--<div class="col-md-8">--}}
                        {{--<input class="form-control" name="password_confirmation" value="" type="password">--}}
                    {{--</div>--}}

                {{--</div>--}}
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                        <input class="btn btn-primary" value="Save Changes" type="submit">
                        <span></span>
                        <input class="btn btn-default" value="Cancel" type="submit">
                    </div>
                </div>

            </form>
            @endforeach
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
</div>
</div>

<hr>


    @stop