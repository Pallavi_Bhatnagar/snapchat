@extends('layout')
@section('title')
    Profile Page
@stop
@section('css')
@stop
@section('content')
    <div class="row">

        <div class="panel panel-default">
            <div class="panel-body">what's your status today ?</div>
            {{--<input type="text" class="form-control">--}}
        </div>

    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="widget-container">
                <div class="widget">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">

                                @foreach($images as $imagesList)
                                    <img src="{{url($imagesList->imagePath)}}">
                                @endforeach

                            </div>
                            <div class="header-name">
                            </div>
                        </div>

                        <div class="panel panel-default">

                        </div>


{{--                                {{ dd('hellooo') }}--}}
                        {!! Form::open(['url'=>'friendsProfile', 'method' => 'get']) !!}
{{--                        {!! Form::open(['url'=>'profilepage/{$search->name}', 'method' => 'get']) !!}--}}

                        {{--<div class="form-group">--}}
                            {{--{!! Form::label('image', 'Choose an image') !!}--}}
                            {{--{!! Form::file('image') !!}--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--{!! Form::submit('upload', array( 'class'=>'btn btn-sm btn-primary' )) !!}--}}
                        {{--</div>--}}

                        {!! Form::close() !!}

                        <div class="card-footer">
                            <div class="footer-social">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="widget-container">
                <div class="widget">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">
                                <img src="http://bootdey.com/img/Content/avatar/avatar6.png" alt="claudio">
                            </div>
                            <div class="header-name">
                                Caludio Morrison
                            </div>
                            {{--<span id="counter">60</span>--}}

                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="footer-social">
                            <div class="row">
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-container">
                <div class="widget">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">
                                <img src="http://bootdey.com/img/Content/avatar/avatar7.png" alt="anna">
                            </div>
                            <div class="header-name">
                                Markez Chan
                            </div>


                        </div>
                        <div class="card-body bg-info">
                            <div class="body-description">


                                <b> "your Friend List"</b> <br>
                                {{--@foreach($friendRequest as $friendAccept)--}}
                                    {{--{{ $friendAccept->friendName }}<br>--}}
                                {{--@endforeach--}}
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="footer-social">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

@stop

