@inject('imageObj', 'App\imageStore')
@inject('picObj', 'App\ACME\UserHelper')
@extends('layout')
@section('title')
    Profile Page
@stop
@section('css')
    <link rel="stylesheet" href="{{ url('css/typeaheadjs.css') }}">
@stop
@section('content')
    <div class="row">
        {{csrf_field()}}
        <div class="panel panel-default">
            <div class="panel-body">what's your status today ?</div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="widget-container">
                <div class="widget" style="background-color: lightyellow">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">
                                @foreach($images as $imagesList)
                                    <img src="{{url($imagesList->imagePath)}}">
                                @endforeach

                            </div>
                            <div class="header-name">
                            </div>
                        </div>

                        {!! Form::open(['url'=>'profilePage/update', 'files'=>true]) !!}

                        <div class="form-group">
                            {!! Form::label('image', 'Choose an image') !!}
                            {!! Form::file('image') !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('upload', array( 'class'=>'btn btn-sm btn-primary' )) !!}
                        </div>

                        {!! Form::close() !!}

                        {{--<div class="card-body bg-primary">--}}
                        <div class="body-description">
                            <b>"Choose your Friends"</b><br>
                            @foreach($friends as $friendsList)
                                {{--@if( !$friendRequestId->pluck('friend_id',\Illuminate\Support\Facades\Auth::user()->id)->contains($friendsList->id))--}}
                                <a class="bold">
                                    <img src="{{$imageObj->fetchImageViaUserId($friendsList->id)}}" width="40"
                                         height="40">{{ $friendsList->name }}</a><a class="btn btn-sm btn-primary"
                                                                                    href="{{ url('friendrequest') }}/{{ $friendsList->id }}/{{ $friendsList->name }}">Add
                                    Friends</a>
                                <br>

                            @endforeach
                            <hr>
                            <a href="{{ url('showusers') }}">see all</a>
                        </div>

                        <div class="card-footer">
                            <div class="footer-social">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="widget-container">
                <div class="widget" style="background-color: lightyellow">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">
                                <img src="http://bootdey.com/img/Content/avatar/avatar6.png" alt="claudio">
                            </div>
                            <div class="header-name">
                            </div>
                        </div>
                        <div class="body-description">
                            <b style="color: #090909">{{ 'Your Messages:' }}</b>
                            @foreach($message as $showmessages)
                                <div class="alert alert-info fade in">
                                    @if($showmessages->To_user_id == \Illuminate\Support\Facades\Auth::user()->id)
                                        <span class="bold" style="color:black;">
                                            <img src="{{$imageObj->fetchImageViaUserId($showmessages->user_id)}}"
                                                 width="30" height="30">
                                            <?php echo '@' ?>{{ $showmessages->sender_name }}:

                                        </span>

                                        <span class="bold" style="color:blue;">
                                            {!! nl2br(e($showmessages->messages)) !!}
{{--                                            {{dd($picObj->getFriendPicAsMessage($showmessages->user_id))}}--}}
                                            <img src="{{$picObj->getFriendPicAsMessage($showmessages->id,$showmessages->To_user_id)}}"
                                                 width="200" height="200">
                                            {{--{{dd($picObj->getFriendPicAsMessage($showmessages->user_id))}}--}}
                                            {{--{{dd($showmessages->pics_path)}}--}}
                                        </span>
{{--                                        <a onclick="setDuration({{$showmessages->set_duration}})" href="{{url('delete/read/'.$showmessages->id)}}" data-toggle="modal"--}}

                                        {{--<a onclick="setDuration({{$showmessages->set_duration}})" data-toggle="modal"--}}
                                        <a onclick="closeModal('{{$showmessages->set_duration}}','{{$showmessages->id}}')" data-toggle="modal"
                                           data-target="#myModal{{ $showmessages->id }}" class="btn btn-default">Read</a>
{{--                                    {{dd( $showmessages->id )}}--}}

                                        <br>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                        @foreach($message as $showmessages)
                            <div class="modal fade" id="myModal{{$showmessages->id}}" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                    data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Modal Header</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>{{$showmessages->messages}}</h3>
{{--                                            <h3>{{$showmessages->id}}</h3>--}}
                                        </div>
                                        <div><span id="counter">3</span></div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        @endforeach
                    </div>
                    <div class="card-footer">
                        <div class="footer-social">
                            <div class="row">
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-container">
                <div class="widget" style="background-color: lightyellow">
                    <div class="profile-card">
                        <div class="card-header">
                            <div class="header-avatar">
                                <img src="http://bootdey.com/img/Content/avatar/avatar7.png" alt="anna">
                            </div>
                            <div class="header-name">
                            </div>

                        </div>
                        <h5 class="text-center" style="color: #090909"> Friends List</h5> <br>

                        <div class="row">

                            @foreach($friendRequest as $friendAccept)

                                @if($friendAccept->friend_id == \Illuminate\Support\Facades\Auth::user()->id && $friendAccept->are_friends == 1)
                                    <div class="col-md-12"
                                         style="background-color: #46b8da;border-bottom: 5px solid white;">

                                        <img src="{{$imageObj->fetchImageViaUserId($friendAccept->user_id)}}"
                                             width="30"
                                             height="30">
                                            <span class="bold" style="color: blue">
                                        {{ $friendAccept->user_name }}
                                                <br><br>
                                   </span>
                                    </div>
                                    <hr>

                                @elseif($friendAccept->user_id == \Illuminate\Support\Facades\Auth::user()->id && $friendAccept->are_friends == 1)
                                    <div class="col-md-12"
                                         style="background-color: #46b8da; border-bottom: 5px solid white;">

                                        <img src="{{$imageObj->fetchImageViaUserId($friendAccept->friend_id)}}"
                                             width="30"
                                             height="30">
                                            <span class="bold" style="color: blue">
                                        {{ $friendAccept->friendName}}
                                                <br><br>
                                        </span>
                                    </div>
                                    {{--@else--}}
                                    {{--<span class="bold" style="color:#525252">--}}
                                    {{--You have no friend in list.--}}
                                    {{--</span>--}}
                                @endif
                                {{--@endif--}}
                            @endforeach
                            <hr>
                            <a href="">see all</a>


                        </div>
                        <div class="card-footer">
                            <div class="footer-social">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-dribbble"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--@endif--}}

    {{--</div>--}}

@stop
@section('js')
    <script src="{{url('js/countdown.js')}}"></script>
    <script type="text/javascript">
    </script>

    {{--<script src="{{url('js/countdown.js')}}"></script>--}}
    {{--<script>--}}
    {{--$('input.typeahead').typeahead({--}}
    {{--name: 'typeahead',--}}
    {{--remote: document.location.origin + '/snapchat/public/fetch/friends/%QUERY',--}}
    {{--limit: 15--}}
    {{--});--}}
    {{--</script>--}}
    <script>
        function setDuration(setDurationTime) {
            return setDurationTime;
        }
    </script>
    <script type="text/javascript">

        function countdown() {
            var i = document.getElementById('counter');
            i.innerHTML = parseInt(i.innerHTML) - 1;

            if (parseInt(i.innerHTML) == 0) {
                clearInterval(timerId);
                $('.modal').modal('hide');
//            location.reload();

            }
        }

    </script>
    <script>
        function closeModal(duration,messageId) {
            setTimeout(function(){
                $('.modal').modal('hide');
                location.reload();
                window.location = 'delete/read/' + messageId;
            }, duration);
        }
    </script>
@stop
