@extends('layout')
@section('title')
    Signup
@stop
@section('css')
    <style>
        body {
            background-color: #525252;
            /*background-color: #525252;*/
        }

        .centered-form {
            margin-top: 60px;
        }

        .centered-form .panel {
            background: rgba(255, 255, 255, 0.8);
            box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
        }
    </style>
@stop
@section('content')

    {{--<div class="container">--}}
    <form method="post" action="{{ url('auth') }}">
        {!! csrf_field() !!}
        <div class="row centered-form">
            <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please sign up for Bootsnipp
                            <small>It's free!</small>
                        </h3>
                    </div>

                    <div class="panel-body">
                        <form role="form">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">

                                        <input type="text" name="name" id="name" class="form-control input-sm"
                                               placeholder="Full Name">
                                        @if($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="gender" id="gender" class="form-control input-sm"
                                               placeholder="gender">
                                        @if($errors->has('gender'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="address" id="address" class="form-control input-sm"
                                               placeholder="address">
                                        @if($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="phone_no" id="phone_no" class="form-control input-sm"
                                               placeholder="Phone No.">

                                        @if($errors->has('phone_no'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_no') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control input-sm"
                                           placeholder="Email Address">
                                    @if($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="password" name="password" id="password"
                                                   class="form-control input-sm" placeholder="Password">
                                            @if($errors->has('password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="password" name="password_confirmation"
                                                   id="password_confirm" class="form-control input-sm"
                                                   placeholder="Confirm Password">
                                            @if($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <input type="submit" value="Register" class="btn btn-info btn-block">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{--</div>--}}
    </form>

@stop
@section('js')
@stop