@inject('imageObj', 'App\imageStore')
@extends('layout')
@section('title')
    Users List
    @stop
@section('content')
<body>

<div>
    <h2>Choose your Friends:</h2>
    <form action="{{ url('showusers') }}" method="get">
    <div class="panel panel-default">
            @foreach($showUsers as $allUsers)
            <div class="panel-heading">
               <a class="bold"><img src="{{$imageObj->fetchImageViaUserId($allUsers->id)}}" width="40"
                     height="40">{{ $allUsers->name }}</a>
                <a  href="{{ url('friendrequest')}}/{{ $allUsers->id }}/{{ $allUsers->name }}"
                    class="btn btn-sm btn-primary col-md-offset-11">Add Friends</a>
            </div>
               @endforeach
       </div>
        </form>
</div>
@stop

</body>

